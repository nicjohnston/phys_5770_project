#!/usr/bin/env python3
import numpy as np
import scipy
import math
import sys
import matplotlib.pyplot as plt
import csv
from scipy.interpolate import CubicSpline
from scipy import interpolate
from decimal import Decimal

G = 6.67408e-11 # units of m^3 kg^-1 s^-2
#c = 299792458 # units of m/s		#original
c = 2.99792e8# units of m/s			#same as matt
# define r in meters and P in pascals

# let dP/dr be f1 and dm/dr be f2
# u[0] = P and u[1] = m
#f1 = lambda r,u,rho:  -1 * ( rho + u[0] ) * ( u[1] + 4*math.pi*u[0]*r**3 ) / ( r * ( (r/G) - 2 * u[1] ) )
#f1 = lambda r,u,rho: -1.0*( G/r**2 ) * ( rho + u[0]/c**2 ) * ( u[1] + 4*math.pi*(r**2)*u[0]/c**2 ) * ( 1 - 2*G*u[1]/(r*c**2) )**(-1)
f1 = lambda r,u,rho: -1.0*( G * u[1] * rho / r**2 ) * ( 1 + u[0]/(rho*c**2) ) * ( 1 + 4*math.pi*(r**3)*u[0]/(u[1]*c**2) ) * ( 1 - 2*G*u[1]/(r*c**2) )**(-1)
f2 = lambda r,u,rho: 4 * math.pi * r**2 * rho
f = [f1,f2]
#def fPrimeTest2(t,u):
#	 return [ 3*u[0] + 2*u[1] - (2*t**2 + 1)*np.exp(2*t), 4*u[0] + u[1] + (t**2 + 2*t - 4)*np.exp(2*t) ]

def rk4_ode_sys(f, f_rho, rStart, h, iterLimit, initialCond, pressureExitCond):
	m = len(f)
	#h = (b-a)/n
	r = rStart
	wj = np.zeros(m)  #same mapping as u, wj[0]=P, wj[1]=m
	k1 = np.zeros(m)
	k2 = np.zeros(m)
	k3 = np.zeros(m)
	k4 = np.zeros(m)
	#ti = np.zeros(n+1)
	#ti[0] = a
	for j in range(0,m):
		wj[j] = initialCond[j]
	#print(iterLimit,h,m)
	#print("r:", r, "\t wj:", wj)
	
	output = [ ]  # should be r, u[1], u[2]
	iters = 0

	#print(f_rho(wj[0]))
	if debug: print("Initial density:",'{:.4e}'.format(f_rho(wj[0])))

	#while wj[0] > pressureExitCond and iters < iterLimit :
	while iters < iterLimit:
		for j in range(0,m):
			#print(i,j)
			k1[j] = h*f[j](r, wj, f_rho(wj[0]))
		if debug: print("k1: ", k1)
		for j in range(0,m):
			k2[j] = h*f[j](r+h/2, wj + 0.5*k1, f_rho(wj[0]))
		if debug: print("k2: ", k2)
		for j in range(0,m):
			k3[j] = h*f[j](r+h/2, wj + 0.5*k2, f_rho(wj[0]))
		if debug: print("k3: ", k3)
		for j in range(0,m):
			k4[j] = h*f[j](r+h, wj + k3, f_rho(wj[0]))
		if debug: print("k4: ", k4)
		for j in range(0,m):
			wj[j] = wj[j] + (k1[j] + 2*k2[j] + 2*k3[j] + k4[j])/6
		r = r + h
		#print(wj,t)
		if wj[0] <= pressureExitCond:
			print("Exiting due to pressure exit condition")
			break
		output.append([ r, wj[0], wj[1], f_rho(wj[0]) ])
		iters = iters + 1
	return output

#	Define the Equation of State
def getRho(P):
	if P < rhoVsPdata[0,1]:
		return rhoVsPdata[0,0] * (P/rhoVsPdata[0,1])**(3/4)
	else:
		return np.exp(np.asscalar(rhoOfP(np.log(P))))

#	Solve TOV to find radius and mass for given center pressure (initial condition, in pascals)
#	Boundary condition is P(R)=0, which occurs at the stellar surface
def calcRandM(Pc,radialStep):
	#radialStep = 5 #	meters
	#initialPressure = 2.4927e36
	initialMass = 4/3*math.pi*getRho(Pc)*radialStep**3
	print("Initial conditions [Pressure, mass]:",[Pc,initialMass])
	
	#rk4_ode_sys(f,			f_rho,	rStart,		h,			iterLimit,	initialCond [P,m],				pressureExitCond):
	resultEnd = np.array(rk4_ode_sys(f, getRho, radialStep, radialStep, itLmt,		[Pc,initialMass],	0)[-1])
	#convert to km and solar mass
	result = [1e-3, 1, 5.029081e-31, 1]*resultEnd
	return [result[0],result[2],getRho(Pc),Pc,result[1]]

def makeRvsMPlot(PcStart,PcEnd,numPcPts,rStep,writeData=0,showPlot=0):
	PcVals = np.geomspace(PcStart,PcEnd,numPcPts)
	numPts = len(PcVals)
	print("Calculating radius and mass for",numPts," different P_c values")
	RvsMdata = []
	for val in PcVals:
#		tmp = calcRandM(val,rStep)
#		tmp.extend([getRho(val), val])
		RvsMdata.append( calcRandM(val,rStep) )
	RvsMdata = np.array(RvsMdata)
	if showPlot:
		plt.plot(RvsMdata[:,1],RvsMdata[:,0])
		plt.show()
	if writeData != 0:
		np.savetxt(writeData, RvsMdata, delimiter=",", fmt='%.30e')
#	plt.plot(RvsMdata[:,2]*10**(-14),RvsMdata[:,1])
#	plt.show()
	return RvsMdata


########################################################################################################
########################################################################################################

debug = 0

#import data and setup interpolation
filename = "rhoPdata3.csv"
with open(filename, newline='') as csvfile:
	rhoVsPdata = np.array(list(csv.reader(csvfile)))
rhoVsPdata = np.asfarray(rhoVsPdata,float)
pData = np.log(rhoVsPdata[:,1]*0.1)
rhoData = np.log(rhoVsPdata[:,0]*1000)
rhoVsPdata = [1000,0.1]*rhoVsPdata #unit conversion
#rhoVsPdata = np.log(rhoVsPdata) #convert to log log axes
rhoOfP = CubicSpline(pData,rhoData, extrapolate=False)
#rhoOfP = interpolate.interp1d(pData,rhoData,fill_value="extrapolate")

radialStep = 1 #	meters
#initialPressure = 2.4927e36
initialPressure = 1.3825e34 #pascals
initialMass = 4/3*math.pi*getRho(initialPressure)*radialStep**3
print("Initial conditions [Pressure, mass]:",[initialPressure,initialMass])

#set itteration limit from cmd line:
itLmt = int(sys.argv[1])


#rk4_ode_sys(f,			f_rho,	rStart,		h,			iterLimit,	initialCond [P,m],				pressureExitCond):
result = rk4_ode_sys(f, getRho, radialStep, radialStep, itLmt,		[initialPressure,initialMass],	0)
result = np.array(result)
#convert to km and solar mass
result = [1e-3, 1, 5.029081e-31, 1]*result
print(result)

args = len(sys.argv) - 1
if args == 2 and int(sys.argv[2]) == 1:
	plt.figure(1)
	#plt.plot(result[:,0],result[:,1])
	plt.plot(result[:,0],result[:,2])
	plt.title(r'Mass and Pressure vs Radius for $P_c=1.3825e34$')
	plt.xlabel('Radius [km]')
	plt.ylabel(r'Mass [${M_\odot}$]')
	#plt.plot(result[:,0],result[:,3])
	#plt.loglog(result[:,1],result[:,3],':rx')
	#plt.loglog(rhoVsPdata[:,1]*0.1,rhoVsPdata[:,0]*1000,'--bo')
	plt.show()
	#plt.figure(2)
	#newP = np.arange(6.2150e29,2e33,1e32)
	#newP = np.exp(np.arange(63.2,82.6,0.1))
	#newRho = np.exp(rhoOfP(np.log(newP)))
	#ytmp = np.exp(rhoOfP(np.log(np.exp(xtmp))))
	#plt.loglog(newP,newRho,'--bo')
	#plt.plot(rhoVsPdata[:,1],rhoVsPdata[:,0],'--bo')
	#plt.show()
	fig, ax1 = plt.subplots()
	color = 'tab:red'
	ax1.set_ylabel('Pressure [Pascals]', color=color,fontsize=16)
	ax1.set_xlabel('Radius [km]',fontsize=16)
	ax1.plot(result[:,0], result[:,1], color=color)
	ax1.tick_params(axis='y', labelcolor=color)
	ax2 = ax1.twinx()
	color = 'tab:blue'
	ax2.set_ylabel(r'Mass [${M_\odot}$]',color=color,fontsize=16)
	ax2.plot(result[:,0], result[:,2], color=color)
	ax2.tick_params(axis='y', labelcolor=color)
	fig.tight_layout()
	plt.show()
	

if args == 2 and int(sys.argv[2]) == 2:
	print("hello")
	#makeRvsMPlot(1e31,2.4e35,424,10,"data_1e31_to_2-4e35_424steps_10m-rstep.csv")
	#makeRvsMPlot(2e32,2.4e35,314,1,"data_2e32_to_2-4e35_314steps_1m-rstep.csv")
	makeRvsMPlot(5e31,2e32,70,1,"data_5e31_to_2e32_70steps_1m-rstep.csv")
