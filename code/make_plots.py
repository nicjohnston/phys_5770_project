#!/usr/bin/env python3
import numpy as np
import scipy
import math
import sys
import matplotlib.pyplot as plt
import csv
from scipy.interpolate import CubicSpline
from scipy import interpolate
from decimal import Decimal

np.set_printoptions(linewidth=150)

with open(sys.argv[1], newline='') as csvfile:
    data=np.array(list(csv.reader(csvfile)))

data=np.asfarray(data,float)
dataLen = len(data)

plotData = []

deletedCounter = 0

for i, row in enumerate(data):
    radius = row[0]
    mass = row[1]
    rho_c = row[2]
    P_c = row[3]
    P_R = row[4]

    PcMin = float(sys.argv[2])
    PcMax = float(sys.argv[3])
    
    #apply cuts:
    if P_c >= PcMin and P_c <= PcMax:
        plotData.append(row)
    else:
        deletedCounter += 1
#    else:
#        print("Throwing out Pc=",P_c)

plotData = np.array(plotData)
print("done cutting data, ", deletedCounter, " out of", len(data), " datapoints deleted")

plt.plot(plotData[:,1],plotData[:,0])
plt.xlabel(r'M [${M_\odot}$]', fontsize=16)
plt.ylabel(r'R [km]', fontsize=16)
plt.title(r'Radius vs Mass')
plt.show()

