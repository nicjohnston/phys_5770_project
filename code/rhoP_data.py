#!/usr/bin/env python3
import numpy as np
import scipy
import math
import sys
import matplotlib.pyplot as plt
import csv
from scipy.interpolate import CubicSpline

filename = "rhoPdata3.csv"

with open(filename, newline='') as csvfile:
    rhoVsPdata = np.array(list(csv.reader(csvfile)))

#rhoVsPdata.astype(np.float)
rhoVsPdata = np.asfarray(rhoVsPdata,float)
rhoOfP = CubicSpline(rhoVsPdata[:,1],rhoVsPdata[:,0])
#print(rhoVsPdata)

plt.loglog(rhoVsPdata[:,0],rhoVsPdata[:,1],'--bo')

#cs = CubicSpline(rhoVsPdata[:,0],rhoVsPdata[:,1])
#newrhovals = np.arange(3.4951e11,4.0498e15,1e12)
#newPvals = cs(newrhovals)
#plt.loglog(newrhovals,newPvals,':rx')

#newP = np.arange(6.2150e29,2.4947e36,1e33)
newP = np.arange(6.2150e29,2e33,1e28)
newRho = rhoOfP(newP)
plt.loglog(newRho,newP,':rx')


plt.show()



